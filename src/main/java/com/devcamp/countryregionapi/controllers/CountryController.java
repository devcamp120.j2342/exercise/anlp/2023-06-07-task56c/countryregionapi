package com.devcamp.countryregionapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.models.Country;
import com.devcamp.countryregionapi.services.CountryService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class CountryController {
    @Autowired
    private CountryService countryservice;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = countryservice.getAllCountries();

        return allCountry;
    }

    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(name="code", required = true) String countryCode){
        ArrayList<Country> allCountry = countryservice.getAllCountries();

        Country findCountry = new Country();

        for (Country countryElement : allCountry) {
            if(countryElement.getCountryCode().equals(countryCode)){
                findCountry = countryElement;
            }
        }
        return findCountry;
    }

}
