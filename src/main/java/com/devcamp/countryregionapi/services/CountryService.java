package com.devcamp.countryregionapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionservice;
    
    Country vietNam = new Country("VN", "Viet Nam");
    Country us = new Country("US", "My");
    Country japan = new Country("JP", "Nhat Ban");

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = new ArrayList<Country>();
        
        vietNam.setRegions(regionservice.getRegionVietNam());
        us.setRegions(regionservice.getRegionUS());
        japan.setRegions(regionservice.getRegionJapan());

        allCountry.add(vietNam);
        allCountry.add(us);
        allCountry.add(japan);

        return allCountry;
    }
}
