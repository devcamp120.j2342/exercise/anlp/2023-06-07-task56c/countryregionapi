package com.devcamp.countryregionapi.services;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.models.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN", "Ha Noi");
    Region tpHoChiMinh = new Region("HCM", "Tp Ho Chi Minh");
    Region daNang = new Region("DN", "Da Nang");

    Region newyork = new Region("NY", "New York");
    Region florida = new Region("FLO", "Florida");
    Region texas = new Region("TX", "Texas");

    Region hokkaido = new Region("HO", "Hokkaido");
    Region kansai = new Region("KS", "Kansai");
    Region kanto = new Region("KT", "Kanto");

    public ArrayList<Region> getRegionVietNam(){
        ArrayList<Region> regionVietName = new ArrayList<Region>();

        regionVietName.add(hanoi);
        regionVietName.add(tpHoChiMinh);
        regionVietName.add(daNang);

        return regionVietName;
    }

    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionUS = new ArrayList<Region>();

        regionUS.add(newyork);
        regionUS.add(florida);
        regionUS.add(texas);

        return regionUS;
    }

    public ArrayList<Region> getRegionJapan(){
        ArrayList<Region> regionJapan = new ArrayList<Region>();

        regionJapan.add(hokkaido);
        regionJapan.add(kansai);
        regionJapan.add(kanto);

        return regionJapan;
    }

    //C2 don het viec cho service
    public Region filterRegion(String regionCode){
        ArrayList<Region> region = new ArrayList<Region>();

        region.add(hanoi);
        region.add(tpHoChiMinh);
        region.add(daNang);
        region.add(newyork);
        region.add(florida);
        region.add(texas);
        region.add(hokkaido);
        region.add(kansai);
        region.add(kanto);

        Region findRegion = new Region();

        for (Region regionElement : region) {
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
        }

        return findRegion;
    }

}
